import { deleteAction, getAction, downFile, getFileAccessHttpUrl, postAction, putAction } from '@/api/manage'
import Vue from 'vue'

export const EditFormMixin = {
  data() {
    return {}
  },
  methods: {
    //新增
    add(data) {
      if (!this.url.add) {
        this.$message.error('未找到url.add参数，请检查')
        return
      } else {
        postAction(this.url.add, data).then(res => {
          if (res.success) {
            this.$message.success(res.message)
            this.visible = false
            this.$emit('reloadPage')
          } else {
            this.$message.error(res.message)
          }
        })
      }
    },
    //修改
    edit(data) {
      if (!this.url.update) {
        this.$message.error('未找到url.update参数，请检查')
        return
      } else {
        putAction(this.url.update, data).then(res => {
          if (res.success) {
            this.$message.success(res.message)
            this.visible = false
            this.$emit('reloadPage')
          } else {
            this.$message.error(res.message)
          }
        })
      }
    }
  }
}