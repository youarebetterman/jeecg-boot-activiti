package org.jeecg.common.constant.enums;

import lombok.Getter;

/**
 * 工作流模型描述信息
 *
 * @author luozhiwei
 * @date 2020/11/17 18:47
 */
@Getter
public enum ActModelNodeEnum {


    MODEL_ID("modelId"),
    MODEL_NAME("name"),
    MODEL_REVISION("revision"),
    MODEL_DESCRIPTION("description");

    private String content;

    ActModelNodeEnum(String content) {
        this.content = content;
    }
}
