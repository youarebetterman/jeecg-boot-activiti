package org.jeecg.common.domain.res;

import lombok.Data;

/**
 * @author luozhiwei
 * @date 2020/11/20 15:47
 */
@Data
public class ProcessDefinitionData {

    private String modelEditorSource;

    private String modelEditorSourceExtra;
}
