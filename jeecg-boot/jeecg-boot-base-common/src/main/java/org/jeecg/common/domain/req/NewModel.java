package org.jeecg.common.domain.req;

import lombok.Data;

/**
 * @author luozhiwei
 * @date 2020/11/17 19:25
 */
@Data
public class NewModel {

    private String id;
    private String name;
    private String key;
    private String category;
    private String des;
}
