package org.jeecg.common.domain.req.vo;

import lombok.Data;

/**
 * Bpmn数据对象
 *
 * @author luozhiwei
 * @date 2020/11/20 11:11
 */
@Data
public class BpmnReqVo {

    private String modelId;

    private String bpmnStr;

    private String svgStr;

    private String key;

    private String name;

    private String category;

    private String des;
}
