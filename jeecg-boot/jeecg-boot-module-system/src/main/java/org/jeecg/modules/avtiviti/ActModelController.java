package org.jeecg.modules.avtiviti;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.activiti.engine.repository.Model;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.domain.req.NewModel;
import org.jeecg.common.domain.req.vo.BpmnReqVo;
import org.jeecg.common.domain.res.ProcessDefinitionData;
import org.jeecg.util.ActUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author luozhiwei
 * @date 2020/11/17 18:38
 */
@RestController
@RequestMapping("/act/model")
@Api(tags = "工作流")
public class ActModelController {

    @Autowired
    private ActUtil actUtil;

    @GetMapping("/page")
    @ApiOperation("工作流模型分页")
    @AutoLog
    public Result<Page<Model>> page(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                    HttpServletRequest req) {
        Result<Page<Model>> pageResult = new Result<>();
        pageResult.setResult(actUtil.getModelPage(pageNo, pageSize, req.getParameterMap()));
        return pageResult;
    }

    @PostMapping("/createModel")
    @ResponseBody
    @ApiOperation("创建工作流模型")
    @AutoLog
    public Result createModel(@RequestBody NewModel newModel) {
        if (actUtil.createModel(newModel)) {
            return Result.ok("操作成功");
        } else {
            return Result.error("操作失败");
        }
    }

    @PutMapping("/updateModel")
    @ApiOperation("更新工作流模型")
    @AutoLog
    public Result updateModel(@RequestBody NewModel newModel) {
        if (actUtil.updateModel(newModel)) {
            return Result.ok("操作成功");
        } else {
            return Result.error("操作失败");
        }
    }

    @DeleteMapping("/deleteModel")
    @ApiOperation("删除工作模型")
    @AutoLog
    public Result deleteModel(String id) {
        if (actUtil.deleteModel(id)) {
            return Result.ok("操作成功");
        } else {
            return Result.error("操作失败");
        }
    }

    @GetMapping("/loadProcessJson")
    @ApiOperation("加载模型json数据")
    @AutoLog
    public Result<ObjectNode> loadProcessJson(String modelId) {
        return Result.ok(actUtil.loadProcessJson(modelId));
    }


    /**
     * 通过modelId查询model数据
     *
     * @param id
     * @return
     */
    @GetMapping("/getModelById")
    @ApiOperation("通过id查询model数据")
    @AutoLog
    public Result<Model> getModelById(@RequestParam(value = "id") String id) {

        return Result.ok(actUtil.getModelById(id));
    }

    /**
     * 保存流程定义数据
     */
    @PostMapping("/saveProcessDefinitionData")
    @ApiOperation("保存流程数据")
    @AutoLog
    public Result saveProcessDefinitionData(@RequestBody BpmnReqVo bpmnReqVo) {
        try {
            actUtil.saveProcessDefinitionData(bpmnReqVo.getModelId(),
                    bpmnReqVo.getKey(),
                    bpmnReqVo.getCategory(),
                    bpmnReqVo.getName(),
                    bpmnReqVo.getBpmnStr(),
                    bpmnReqVo.getSvgStr());
            return Result.ok("操作成功");
        } catch (Exception e) {
            return Result.error(e.getMessage());
        }
    }

    @GetMapping("getProcessDefinitionData")
    @ApiOperation("/获取流程定义数据")
    @AutoLog
    public Result getProcessDefinitionData(@RequestParam("modelId") String modelId) {
        ProcessDefinitionData processDefinitionData = actUtil.getProcessDefinitionData(modelId);
        return Result.ok(processDefinitionData);
    }

    @GetMapping("deployProcess")
    @ApiOperation("部署流程")
    @AutoLog
    public Result deployProcess(@RequestParam("modelId") String modelId) {
        actUtil.deployProcess(modelId);
        return Result.ok("部署成功");
    }
}
